app.controller('HomeController', function($scope, $location, session, storage) {
	$scope.usuario = {};
	$scope.message = "";
      
	$scope.submitForm = function(usuario) {
		var existeusuario;

		$scope.message = "";
			   
	    			existeusuario = storage.getList("users").filter(function(obj) {
			  	return obj.username == usuario.username;
			});
				if(existeusuario[0].password == usuario.password){
				
					session.init(existeusuario[0]);

					if(session.isAdmin()){
						$location.path('/profile');
					
					}else{
						$location.path('/comics');
					}
				}else{
					//Error password
					$scope.message = "No se encuentran el usuario o la contraseña";
				}
	
		
			$scope.usuario = {};	   

	  
	};

});