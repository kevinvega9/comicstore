app.controller('RegisterController', function($scope, $location, session, storage) {
	$scope.usuario = {};
	$scope.message = "";
       
	$scope.registerForm = function(usuario) {
	var existeusuario;

		     	
			existeusuario = storage.getList("users").filter(function(obj) {
			  	return obj.username == usuario.username;
			});

	    		if(existeusuario.length){
	    			$scope.message = "This username already exist. Please choose another.";
	    		}else{
	    			usuario["role"] = "user";

	    			storage.addNew("users", usuario);


	    			$scope.message = "Your account has been created successfully.";
	    			
	    		}

};
	
});