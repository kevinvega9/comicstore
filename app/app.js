var app = angular.module('comicStore', ['ngAnimate', 'ngRoute', 'ui'])

app.run(['$rootScope', '$location', 'session', function ($rootScope, $location, session) {
    $rootScope.$on('$routeChangeStart', function (event) {


    	var path = $location.path();    
        if(!session.logTrue() && path != '/register'){
            $location.path('/');

        }

    });
}]);