app.config(function($locationProvider) {
});

app.config(function($routeProvider) {

  $routeProvider.when('/',{
        controller: 'HomeController',
        templateUrl: './app/components/home/home.html'
  })
      .when('/comics',{
        controller: 'StoreController',
        templateUrl: './app/components/comics/comicView.html'
  })
       .when('/register',{
        controller: 'RegisterController',
        templateUrl: './app/components/home/register.html'
  })
      .when('/profile',{
        controller: 'StoreController',
        templateUrl: './app/components/home/profile.html'
         })
      .when('/users',{
        controller: 'StoreController',
        templateUrl: './app/components/admin/adminUsers.html'
         })
      .when('/adduser',{
        controller: 'RegisterController',
        templateUrl: './app/components/admin/addUser.html'
         })
      .when('/edituser',{
        controller: 'EditUserController',
        templateUrl: './app/components/admin/editUser.html'
         })
       .when('/404',{
        controller: 'StoreController',
        templateUrl: './app/components/home/404.html'
         })

  .otherwise({
        redirectTo: '/404'
        
    });
 
});