app.directive('comicHeader',function(){
  return{
  restrict: 'E',
  templateUrl:'app/shared/main/comic-header.html'
  };
  });

  app.directive('comicFeature',function(){

  	return{
  		restrict: 'E',
  		templateUrl: 'app/shared/main/comic-feature.html'
  	}

  });

    app.directive('comicFooter',function(){

    return{
      restrict: 'E',
      templateUrl: 'app/shared/main/comic-footer.html'
    }

  });

    app.directive('sideBar',function(){

    return{
      restrict: 'E',
      templateUrl: 'app/shared/main/side-bar.html'
    }

  });

       app.directive('navBar',function(){

    return{
      restrict: 'E',
      templateUrl: 'app/shared/main/nav-bar.html'
    }

  });