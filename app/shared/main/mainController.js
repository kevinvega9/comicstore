app.controller('MainController', function($scope, storage){

  if(!storage.getList("users")){
  	storage.init("comics", "./assets/json/comic.json");
  	storage.init("header", "./assets/json/header.json");
    storage.init("users", "./assets/json/users.json");
  }
});


app.factory('storage', function() {
    return {
      init: function(type, url){
        var request = new XMLHttpRequest();
      request.open('GET', url, false);  // `false` makes the request synchronous
      request.send(null);
      
      if (request.status === 200) {
        localStorage[type] = JSON.stringify(JSON.parse(request.responseText));
      }
      
      localStorage[type] = JSON.stringify(JSON.parse(request.responseText));
      },
      getList: function(type) {
            return localStorage[type] !== undefined ? JSON.parse(localStorage[type]) : false;
      },
      addNew: function(type, model) {
          collection = this.getList(type);
            collection.push(model);
            localStorage[type] = JSON.stringify(collection);
        },
        save: function(type, ary){
            localStorage[type] = JSON.stringify(ary);
        },
    };
});

app.factory('session', function(){
  return {
    init: function(user){
      sessionStorage["user"] = JSON.stringify(user);
    },
        
    logTrue: function(){
            user = sessionStorage["user"] !== undefined ? JSON.parse(sessionStorage["user"]) : false;
      return user;
    },
        isAdmin: function() {
            user = this.logTrue();
            return user["role"] == "admin";
        },
        isUser: function() {
            user = this.logTrue();
            return user["role"] == "user";
        }
  };
});
app.controller('OrderController', ['$scope', '$filter', function($scope, $filter) {
    var orderBy = $filter('orderBy');
     $scope.order = function(predicate) {
      $scope.listComics = JSON.parse(localStorage["comics"]);

      $scope.predicate = predicate;
      $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
      $scope.listComics = orderBy($scope.listComics, predicate, $scope.reverse);
    };
   }]);
